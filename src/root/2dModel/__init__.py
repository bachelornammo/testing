"""
GUI
    ChangeVarSize
        gets variables from user
        gets figures based on variables
        displays figures
    quit
        exits prgram on button push

lineSegment
    class line
        __init__
            takes two points and creates a line between them
            finds magnitude, normal and magnitude of normal
        sharedPint
            returns the point if ne is shared, false otherwise
        perp
            returns normal of [[0,0],a] 
        intersect
            returns true if lines share a point
            returns coordinates of intersection otherwise
        moveSegment
            moves the line moveDist along the normal

moveLine
    class mover
        __init__
            defines variables
        createLine
            takes xs and ys and creates an array of line segments from it
        drawStep
            moves line segments and checks if they are all valid
        checkSegment
            check if segments overlap or do not connect
            if so fix it
        set functions
            sets the indicated values
    checkAngle
        takes two lines and returns the angle between them
    find nearest
        finds the index of the closest array element to value
    getPoints
        returns the points in a array of line segments
    isClose
        returns true if two numbers are relatively close together

rotator
    rotationMatrix
        takes xs and ys and number of arms
        mirrors the figure and rotates it to each arm

starShape
    class star
        __init__
            defines variables
        set functions
            sets the indicated values
        starShape
            creates a half arm for a star shape
        tankWall
            creates the tank wall figure
        circle
            returns y value given x and r
                    x value given y and r
"""
