'''
Created on 15. mp1rs 2017

@p1uthor: Jonanders
'''

import numpy as np

def circleCrossing(line, r):
    p1 = line[0]
    p2 = line[1]
    #x^2*|p1|^2 + (x^2-2x+1)*|p2|^2 + 2x(1-x)*p1*p2 = R^2
    #(x^2-2x+1)*|p2|^2 = x^2*|p2|^2 - 2x*|p2|^2 + |p2|^2
    #2x(1-x)*p1*p2 = 2x*p1*p2 - 2x^2*p1*p2
    #x^2*p1^2 + x^2*p2^2 - 2x*p2^2 + p2^2 + 2x*p1*p2 - 2x^2*p1*p2 -R^2 = 0
    # = x^2*|p1|^2 + x^2*|p2|^2 - 2x^2*p1*p2 -2x*|p2|^2 + 2x*p1*p2 + |p2|^2 - R^2
    
    dotP1 = abs(p1[0]*p1[0] + p1[1]*p1[1])
    dotP2 = abs(p2[0]*p2[0] + p2[1]*p2[1])
    dotP1P2 = p1[0]*p2[0] + p1[1]*p2[1]
    #x^2*|p1|^2 + x^2*|p2|^2 - 2x^2*p1*p2
    a = dotP1 + dotP2 - 2*dotP1P2
    #-2x*|p2|^2 + 2x*p1*p2
    b = -2*dotP2 + 2*dotP1P2
    #|p2|^2 - R^2
    c = dotP2 - pow(r,2)
    
    #x = (-b+-sqrt(b^2-4ac) / 2a
    alpha1 = (-b + np.sqrt(pow(b,2)-4*a*c)) / (2*a)
    alpha2 = (-b - np.sqrt(pow(b,2)-4*a*c)) / (2*a)
    print "alpha1: ", alpha1
    print "alpha2: ", alpha2
    if -1 <= alpha1 <= 1:
        return alpha1
    else:
        return alpha2

a = [1,0]
b = [1,2]
line = [a, b]
x = circleCrossing(line, 2)
#x*p1 + (1-x)*p2
crossing = [x*a[0] + (1-x)*b[0],
            x*a[1] + (1-x)*b[1]]
print crossing