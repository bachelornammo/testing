'''
Created on 20. feb. 2017

@author: JonAnders
'''
"""
import numpy
import matplotlib.pyplot as pp
from numpy import empty, log, abs
from numpy.lib.scimath import sqrt
from scipy.optimize import fsolve
from sympy import solve, Symbol
"""
"""
def circle(x):
    return abs(sqrt(pow(20,2)-pow(x,2)))
def line(x):
    return log(x)

def functions(x):    
    F = empty((2))
    F[0] = abs(sqrt(pow(20,2)-pow(x,2)))
    F[1] = log(x)
    print x
    return F

zGuess = [11.0]
z = empty((2))
z[0] = fsolve(lambda x: abs(sqrt(pow(20,2)-pow(x,2))) - log(x), zGuess)
#z[0] = fsolve(functions, zGuess)
z[1] = line(z[0])
print z
x_axisOne = []
y_axisOne = []
x_axisTwo = []
y_axisTwo = []
for x in range(-21,21):
    x_axisOne.append(x)
    y_axisOne.append(line(x))
    x_axisTwo.append(x)
    y_axisTwo.append(circle(x))
    
pp.plot(x_axisOne, y_axisOne)
pp.plot(x_axisTwo, y_axisTwo)
pp.plot(z[0], z[1], 'go')
pp.grid('on')
pp.show()
"""
"""
xOne = [100, 400]
yOne = [240, 265]

xTwo = [100, 400]
yTwo = [265, 240]

coefficients = numpy.polyfit(xOne, yOne, 1)
polynomial = numpy.poly1d(coefficients)
x_axisOne = numpy.linspace(0,500,100)
y_axisOne = polynomial(x_axisOne)

coefficientsTwo = numpy.polyfit(xTwo, yTwo, 1)
polynomialTwo = numpy.poly1d(coefficientsTwo)
x_axisTwo = numpy.linspace(0,500,100)
y_axisTwo = polynomialTwo(x_axisTwo)

x = Symbol('x')
z = solve(polynomial - polynomialTwo)
print z
pp.plot(x_axisOne, y_axisOne)
pp.plot( xOne[0], yOne[0], 'go' )
pp.plot( xOne[1], yOne[1], 'go' )
pp.plot(x_axisTwo, y_axisTwo)
pp.plot( xTwo[0], yTwo[0], 'go' )
pp.plot( xTwo[1], yTwo[1], 'go' )
pp.grid('on')
pp.show()
"""

from numpy import *
def perp( a ) :
    b = empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
# return 
def seg_intersect(a1,a2, b1,b2) :
    da = a2-a1

    db = b2-b1
    dp = a1-b1
    dap = perp(da)
    
#    print "da = ", da           print "a1 = ", self.oldp
#    print "db = ", db           print "a2 = ", self.p
#    print "dp = ", dp           print "b1 = ", b.oldp
#    print "dap = ", dap         print "b2 = ", b.p
    denom = dot( dap, db)
    if denom != 0:
        num = dot( dap, dp )
        return (num / denom.astype(float))*db + b1
    return False

p1 = array( [0.0, 0.0] )
p2 = array( [2.0, 2.0] )

p3 = array( [0.0, 2.0] )
p4 = array( [2.0, 0.0] )

print seg_intersect( p1,p2, p3,p4)

p1 = array( [2.0, 2.0] )
p2 = array( [3.0, 3.0] )

p3 = array( [1.0, 0.0] )
p4 = array( [2.0, 1.0] )

#print seg_intersect( p1,p2, p3,p4)