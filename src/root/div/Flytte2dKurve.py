'''
Created on 7. feb. 2017

@author: JonAnders
'''

import numpy
import matplotlib.pyplot as pp

r = 3
stepSize = 1
moveOnX = 7

x_window = -10 , 10
y_window = -10, 10

xs = []
ys = []
xTank = []
yTank = []

def lineFunction(x):
    return numpy.sqrt(pow(r,2)-pow(x,2))

def tankWall(x):
    return numpy.sqrt(pow(7,2)-pow(x,2))
def tankWallByY(y):
    return numpy.exp(-3 + y) - moveOnX

def moveEndpoint(XS, YS, X):
    #check if endpoint is undefined by tankWall
    if (X <= -moveOnX):
        XS.append(tankWallByY(0))
        YS.append(0)
    else:
        XS.append(X)
        if (tankWall(X) < 0):
            YS.append(tankWall(X))
        else:
            YS.append(0)
    print XS[XS.__len__()-1], YS[YS.__len__()-1]

def drawInitial():
    xs.append(-r)
    ys.append(0)
    for x in numpy.linspace(*x_window, num=2000):
        try:
            # A more efficient technique would use the last-found-y-value as a 
            # starting point
            y = lineFunction(x)
            yT = tankWall(x)
        except ValueError:
            # Should we not be able to find a solution in this window.
            pass
        else:
            if (~numpy.isnan(y)):
                xs.append(x)
                ys.append(y)
            xTank.append(x)
            yTank.append(yT)
    for x in numpy.linspace(x_window[1], x_window[0], num=2000):
        try:
            # A more efficient technique would use the last-found-y-value as a 
            # starting point
            y = lineFunction(x)
            y = -y
        except ValueError:
            # Should we not be able to find a solution in this window.
            pass
        else:
            if (~numpy.isnan(y)):
                xs.append(x)
                ys.append(y)
    xs.append(-r)
    ys.append(0)
    pp.plot(xs, ys)
    pp.plot(xTank, yTank)

vector = []
xs2 = []
ys2 = []

def drawStep():
    prevNormal = [0, 0]
    for x in range(0, xs.__len__()):
        #find vector
        vector.append([xs[x], ys[x]])
        #compute normal
        if (x > 0):
            dx = vector[x][0] - vector[x-1][0]
            dy = vector[x][1] - vector[x-1][1]
            normal = [-dy, dx]
            
            #do for from 2. normal onwards
            if (prevNormal != [0, 0]):
                #average previous normal with this one
                avgNormal = [(vector[x][0] + vector[x-1][0])/2, 
                             (vector[x][1] + vector[x-1][1])/2]
                length = numpy.sqrt(avgNormal[0]**2 + avgNormal[1]**2)
                #normalize length and multiply by desired step size
                avgNormal[0] = (avgNormal[0] / length) * stepSize
                avgNormal[1] = (avgNormal[1] / length) * stepSize
                
                #new point
                newX = xs[x-1] + avgNormal[0]
                newY = ys[x-1] + avgNormal[1]
                
                #check if valid Y value
                if (newY > tankWall(newX)):
                    newY = tankWall(newX)
                
                xs2.append(newX)
                ys2.append(newY)
                
            #save normal
            prevNormal = normal
        else:
            initialX = (xs[x] - 1) * stepSize
            moveEndpoint(xs2, ys2, initialX)
    moveEndpoint(xs2, ys2, initialX)
    pp.plot(xs2, ys2)

drawInitial()

for x in range(1, 5):
    drawStep()
    xs = xs2
    ys = ys2
    xs2 = []
    ys2 = []
pp.xlim(*x_window)
pp.ylim(*y_window)
pp.show()