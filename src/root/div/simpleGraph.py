'''
Created on 18. jan. 2017

@author: Mike Graham

@source: http://stackoverflow.com/questions/2484527/
         is-it-possible-to-plot-implicit-equations-using-matplotlib
'''
from functools import partial
import numpy
import matplotlib.pyplot as pp
import scipy.optimize


def z(x, y):
    return x ** 2 + x * y + y ** 2 - 10

x_window = 0, 5
y_window = 0, 10

xs = []
ys = []
for x in numpy.linspace(*x_window, num=200):
    try:
        # A more efficient technique would use the last-found-y-value as a 
        # starting point
        y = scipy.optimize.brentq(partial(z, x), *y_window)
    except ValueError:
        # Should we not be able to find a solution in this window.
        pass
    else:
        xs.append(x)
        ys.append(y)

pp.plot(xs, ys)
pp.xlim(*x_window)
pp.ylim(*y_window)
pp.show()
    

