'''
Created on 22. feb. 2017

@author: JonAnders
'''
import numpy as np
import matplotlib.pyplot as plt

#x = r*cos*theta
#y = r*sin*theta
#theta 6.28 = theta 0
def line(theta, k):
    r = (k / np.sin(theta))
    return [theta, r]

outerR = 2
innerR = 1

ax = plt.subplot(111, projection='polar')
ax.plot(line(innerR, outerR))
ax.set_rmax(2)
ax.set_rticks([0.5, 1, 1.5, 2])  # less radial ticks
ax.set_rlabel_position(-22.5)  # get radial labels away from plotted line
ax.grid(True)

ax.set_title("A figure created using symmetry lines\n", va='bottom')
plt.show()