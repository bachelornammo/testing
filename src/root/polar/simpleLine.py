'''
Created on 22. feb. 2017

@author: JonAnders
'''
import numpy as np
import matplotlib.pyplot as plt

numberOfRounds = 1
r = np.arange(0, 2, 0.01)
theta = numberOfRounds * np.pi * r
print theta
ax = plt.subplot(111, projection='polar')
ax.plot(theta, r)
ax.set_rmax(2)
ax.set_rticks([0.5, 1, 1.5, 2])  # less radial ticks
ax.set_rlabel_position(-22.5)  # get radial labels away from plotted line
ax.grid(True)

ax.set_title("A line plot on a polar axis", va='bottom')
plt.show()