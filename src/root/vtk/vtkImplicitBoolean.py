'''
Created on 31. jan. 2017

@author: JonAnders
'''
import vtk

sphere = vtk.vtkSphere()
sphere.SetRadius(1)
sphere.SetCenter(1, 0, 0)

innerCylinder = vtk.vtkCylinder()
innerCylinder.SetRadius(0.5)
innerCylinder.SetCenter(1, 0, 0)

boolean = vtk.vtkImplicitBoolean()
boolean.SetOperationTypeToDifference()
#boolean.SetOperationTypeToIntersection()
#boolean.SetOperationTypeToUnion()
#boolean.AddFunction(innerCylinder)
boolean.AddFunction(sphere)
boolean.AddFunction(innerCylinder)

sample = vtk.vtkSampleFunction()
sample.SetImplicitFunction(boolean)
sample.SetModelBounds(-1, 2, -1, 1, -1, 1)
sample.SetSampleDimensions(40, 40, 40)
sample.ComputeNormalsOff()

surface = vtk.vtkContourFilter()
surface.SetInputConnection(sample.GetOutputPort())
surface.SetValue(0, 0.0)

mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(surface.GetOutputPort())
mapper.ScalarVisibilityOff()

actor = vtk.vtkActor()
actor.SetMapper(mapper)
actor.GetProperty().EdgeVisibilityOn()
actor.GetProperty().SetEdgeColor(.2, .2, .5)

ren = vtk.vtkRenderer()
ren.SetBackground(1, 1, 1)
ren.AddActor(actor)

renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
iren.Initialize()
iren.Start() 