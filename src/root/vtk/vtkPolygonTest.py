'''
Created on 31. jan. 2017

@author: JonAnders
'''
import vtk
from numpy import arange
from math import sqrt

#x^2 + y^2 = r^2    sirkel

#r = input("Enter radius: ")
r = 3
l = 10
points = vtk.vtkPoints()
longPoints = vtk.vtkPoints()
sidePoints = vtk.vtkPoints()

count = 0
longCount = 0
sideCount = 0
slices = vtk.vtkCellArray()
longSlices = vtk.vtkCellArray()
sideSlices = vtk.vtkCellArray()
points.InsertNextPoint(0, 0, 0)
longPoints.InsertNextPoint(0, 0, l)

#define first half of circles
for x in arange(-r, r, 0.1):
    #skip first iteration (no prevPoint)
    if (x == -r):
        prevPoint = [x, sqrt(r**2-x**2), 0]
        longPrevPoint = [x, sqrt(r**2-x**2), l]
    else:
        #1. circle
        points.InsertNextPoint(prevPoint)
        thisPoint = [x, sqrt(r**2-x**2), 0]
        points.InsertNextPoint(thisPoint)
        points.InsertNextPoint(0, 0, 0)
        
        circleSlice = vtk.vtkPolygon()
        circleSlice.GetPointIds().SetNumberOfIds(3)
        circleSlice.GetPointIds().SetId(0, count); count += 1
        circleSlice.GetPointIds().SetId(1, count); count += 1
        circleSlice.GetPointIds().SetId(2, count); count += 1
        
        slices.InsertNextCell(circleSlice)
        
        #2. circle
        longPoints.InsertNextPoint(longPrevPoint)
        longThisPoint = [x, sqrt(r**2-x**2), l]
        longPoints.InsertNextPoint(longThisPoint)
        longPoints.InsertNextPoint(0, 0, l)
        
        circleSlice = vtk.vtkPolygon()
        circleSlice.GetPointIds().SetNumberOfIds(3)
        circleSlice.GetPointIds().SetId(0, longCount); longCount += 1
        circleSlice.GetPointIds().SetId(1, longCount); longCount += 1
        circleSlice.GetPointIds().SetId(2, longCount); longCount += 1
        
        longSlices.InsertNextCell(circleSlice)
        
        #side wall
        sidePoints.InsertNextPoint(prevPoint)
        sidePoints.InsertNextPoint(thisPoint)
        sidePoints.InsertNextPoint(longPrevPoint)
        
        sideSlice = vtk.vtkPolygon()
        sideSlice.GetPointIds().SetNumberOfIds(3)
        sideSlice.GetPointIds().SetId(0, sideCount); sideCount += 1
        sideSlice.GetPointIds().SetId(1, sideCount); sideCount += 1
        sideSlice.GetPointIds().SetId(2, sideCount); sideCount += 1
        
        sideSlices.InsertNextCell(sideSlice)
        
        sidePoints.InsertNextPoint(longPrevPoint)
        sidePoints.InsertNextPoint(longThisPoint)
        sidePoints.InsertNextPoint(thisPoint)
        
        sideSlice = vtk.vtkPolygon()
        sideSlice.GetPointIds().SetNumberOfIds(3)
        sideSlice.GetPointIds().SetId(0, sideCount); sideCount += 1
        sideSlice.GetPointIds().SetId(1, sideCount); sideCount += 1
        sideSlice.GetPointIds().SetId(2, sideCount); sideCount += 1
        
        sideSlices.InsertNextCell(sideSlice)
        
        prevPoint = thisPoint
        longPrevPoint = longThisPoint

#define second half of circle      
for x in arange(r, -r-0.1, -0.1):
    #prevent math error in sqrt(0)
    if (str(x) != str(-r + 0.0)):
        y = sqrt(r**2-x**2)
    else:
        y = 0
    #skip first iteration (no prevPoint)
    if (x == -r):
        prevPoint = [x, -y, 0]
        longPrevPoint = [x, -y, l]
    else:
        points.InsertNextPoint(prevPoint)
        prevPoint = [x, -y, 0]
        points.InsertNextPoint(prevPoint)
        points.InsertNextPoint(0, 0, 0)
        
        circleSlice = vtk.vtkPolygon()
        circleSlice.GetPointIds().SetNumberOfIds(3)
        circleSlice.GetPointIds().SetId(0, count)
        count += 1
        circleSlice.GetPointIds().SetId(1, count)
        count += 1
        circleSlice.GetPointIds().SetId(2, count)
        count += 1
        
        slices.InsertNextCell(circleSlice)
        
        #2. circle
        longPoints.InsertNextPoint(longPrevPoint)
        longPrevPoint = [x, -y, l]
        longPoints.InsertNextPoint(longPrevPoint)
        longPoints.InsertNextPoint(0, 0, l)
        
        circleSlice = vtk.vtkPolygon()
        circleSlice.GetPointIds().SetNumberOfIds(3)
        circleSlice.GetPointIds().SetId(0, longCount); longCount += 1
        circleSlice.GetPointIds().SetId(1, longCount); longCount += 1
        circleSlice.GetPointIds().SetId(2, longCount); longCount += 1
        
        longSlices.InsertNextCell(circleSlice)

circlePolyData = vtk.vtkPolyData()
circlePolyData.SetPoints(points)
circlePolyData.SetPolys(slices)

longPolyData = vtk.vtkPolyData()
longPolyData.SetPoints(longPoints)
longPolyData.SetPolys(longSlices)

sidePolyData = vtk.vtkPolyData()
sidePolyData.SetPoints(sidePoints)
sidePolyData.SetPolys(sideSlices)

#clean and map
cCleanPolyData = vtk.vtkCleanPolyData()
cCleanPolyData.SetInput(circlePolyData)
circleMapper = vtk.vtkPolyDataMapper()
circleMapper.SetInputConnection(cCleanPolyData.GetOutputPort())

lCleanPolyData = vtk.vtkCleanPolyData()
lCleanPolyData.SetInput(longPolyData)
longMapper = vtk.vtkPolyDataMapper()
longMapper.SetInputConnection(lCleanPolyData.GetOutputPort())

sCleanPolyData = vtk.vtkCleanPolyData()
sCleanPolyData.SetInput(sidePolyData)
sMapper = vtk.vtkPolyDataMapper()
sMapper.SetInputConnection(sCleanPolyData.GetOutputPort())

#define actors
circleActor = vtk.vtkActor()
circleActor.SetMapper(circleMapper)
circleActor.GetProperty().EdgeVisibilityOn()
circleActor.GetProperty().SetEdgeColor(.2, .2, .5)

longActor = vtk.vtkActor()
longActor.SetMapper(longMapper)
longActor.GetProperty().EdgeVisibilityOn()
longActor.GetProperty().SetEdgeColor(.2, .2, .5)

sideActor = vtk.vtkActor()
sideActor.SetMapper(sMapper)
sideActor.GetProperty().EdgeVisibilityOn()
sideActor.GetProperty().SetEdgeColor(.2, .2, .5)

#render scene
renderer = vtk.vtkRenderer()
renderer.AddActor(circleActor)
renderer.AddActor(longActor)
renderer.AddActor(sideActor)
renderer.SetBackground(.5, .3, .31)

renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(renderer)
renWin.Render()

interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(renWin)
interactor.Initialize()
interactor.Start()