'''
Created on 6. feb. 2017

@author: JonAnders
'''
import vtk
import Tkinter
from vtk.tk.vtkTkRenderWindowInteractor import vtkTkRenderWindowInteractor

#define window
root = Tkinter.Tk()
root.title("Testing window")
frame = Tkinter.Frame(root)
frame.pack( fill=Tkinter.BOTH, expand=1, side=Tkinter.TOP)

#define sphere
sphereSource = vtk.vtkSphereSource()
sphereSource.SetCenter(1, 0, 0)
sphereSource.Update()
inputSphere = sphereSource.GetOutput()
sphereTri = vtk.vtkTriangleFilter()
sphereTri.SetInput(inputSphere)

#define cylinder
cylinderSource = vtk.vtkCylinderSource()
cylinderSource.SetHeight(2)
inputCylinder = cylinderSource.GetOutputPort()

#rotate cylinder
transform = vtk.vtkTransform()
transform.RotateWXYZ(90, 0, 0, 90)
transformFilter = vtk.vtkTransformFilter()
transformFilter.SetTransform(transform)
transformFilter.SetInputConnection(inputCylinder)
transformFilter.Update()

cylTri = vtk.vtkTriangleFilter()
cylTri.SetInput(transformFilter.GetOutput())

#set sphere mapper and actor
sphereMapper = vtk.vtkPolyDataMapper()
sphereMapper.SetInputConnection(inputSphere.GetProducerPort())
sphereMapper.ScalarVisibilityOff()
sphereActor = vtk.vtkActor()
sphereActor.SetMapper(sphereMapper)
sphereActor.GetProperty().SetColor(1,0,0)

#set cylinder mapper and actor
cylMapper = vtk.vtkPolyDataMapper()
cylMapper.SetInputConnection(cylTri.GetOutputPort())
cylMapper.ScalarVisibilityOff()
cylActor = vtk.vtkActor()
cylActor.SetMapper(cylMapper)

#define boolean operation of the two shapes
booleanOperation = vtk.vtkBooleanOperationPolyDataFilter()
booleanOperation.SetOperationToUnion()
booleanOperation.SetInputConnection(0, sphereTri.GetOutputPort())
booleanOperation.SetInputConnection(1, cylTri.GetOutputPort())
booleanOperation.Update()

#set boolean mapper and actor
booleanOperationMapper = vtk.vtkPolyDataMapper()
booleanOperationMapper.SetInputConnection(booleanOperation.GetOutputPort())
booleanOperationMapper.ScalarVisibilityOff()
booleanOperationActor = vtk.vtkActor()
booleanOperationActor.SetMapper(booleanOperationMapper)

#define rendering settings
renderer = vtk.vtkRenderer()
renderer.AddViewProp(sphereActor)
renderer.AddViewProp(cylActor)
#renderer.AddViewProp(booleanOperationActor)
renderer.SetBackground(.1, .2, .3)
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer( renderer )
 
#set rendering output to main window
renWinInteractor = vtkTkRenderWindowInteractor(root, rw=renderWindow, width=400, height=400)
renWinInteractor.Initialize()
renWinInteractor.pack(side='top', fill='both', expand=1)

renderWindow.PolygonSmoothingOn()
renderWindow.Render()
root.mainloop()
