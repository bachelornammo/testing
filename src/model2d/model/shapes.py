'''
Created on 27. feb. 2017

@author: JonAnders
'''
import numpy as np
from src.model2d.model import lineSegment
import src.model2d.controller.nonClass as nonClass


#Star with paralell arms
class paraStar:
    
    def __init__(self):        
        self.segments = []

    def setNumArms(self, num):
        self.numArms = num / 2
        self.theta = np.pi / self.numArms
    def setInnerRadius(self, r):
        self.innerR = r
    def setWidth(self, w):
        self.width = w / 2
    def setLength(self, l, tank):
        #can't be longer than outer R
        if (l < tank.outerR):
            self.length = l
        else:
            self.length = tank.outerR
        self.outerR = tank.outerR
    def setPointDistance(self, stepSize):
        self.pointDistance = stepSize / 10
    
    #create one full arm of the star
    def getStarShape(self):
        xs = []
        ys = []
        #rounding between each arm
        if (self.innerR > self.width):
            x1 = self.innerR * np.cos(self.theta / 2)
                #y = sqrt(r^2-x^2) => x = sqrt(r^2-y^2)
            x2 = nonClass.circle(-self.width, self.innerR)
            #from x1 to x2
            for x in np.arange(x1, x2, self.pointDistance):
                y = -nonClass.circle(x, self.innerR)
                if y <= -self.width:
                    ys.append(y)
                    xs.append(x)
        #draw straigth part of arm
        xs.append(x2)
        ys.append(-self.width)
        xs.append(self.length - self.width)
        ys.append(-self.width)
        #draw rounding of arm
        for x in np.arange(self.pointDistance, self.width, self.pointDistance):
            ys.append(-nonClass.circle(x, self.width))
            xs.append(x+self.length-self.width)
        # add last point     
        ys.append(0)
        xs.append(self.length)
        #returns the line segments for the points
        return nonClass.createLine(xs, ys)
                
        
    
    def createLine(self, initXs, initYs):
        prevX = 0
        prevY = 0
        segments = []
        for i in range(1, initXs.__len__()):
            x = initXs[i]
            y = initYs[i]
            prevX = initXs[i-1]
            prevY = initYs[i-1]
            segment = lineSegment.lineSegment([prevX, prevY], [x, y])
            segments.append(segment)
        return segments
        
     
class tank():
    def setOuterRadius(self, r):
        self.outerR = r

    def getTankWall(self):
        tankXs = []
        tankYs = []
        #draw upper half
        for x in np.arange(-self.outerR, self.outerR, 0.1):
            tankYs.append(nonClass.circle(x, self.outerR))
            tankXs.append(x)
        #draw lower half
        for x in np.arange(self.outerR, -self.outerR, -0.1):
            tankYs.append(-nonClass.circle(x, self.outerR))
            tankXs.append(x)
        #close circle
        tankYs.append(0)
        tankXs.append(-self.outerR)

        return nonClass.createLine(tankXs, tankYs)