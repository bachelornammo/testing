'''
Created on 10. mars 2017

@author: JonAnders
'''
import numpy as np
from sys import stderr

class lineSegment:
    def __init__(self, pointA, pointB):
        self.a = pointA
        self.updatePoint('b', pointB)

    def __eq__(self, other):
        self.a = np.array(self.a)
        self.b = np.array(self.b)
        if np.array_equiv(self.a, other.a):
            if np.array_equiv(self.b, other.b):
                return True
        return False
    
    def getX(self):
        return [self.a[0], self.b[0]]
    def getY(self):
        return [self.a[1], self.b[1]]
    
    def updatePoint(self, point, value):
        if (point == 'a'):
            self.a = value
        elif(point == 'b'):
            self.b = value
        else:
            stderr("Invalid input to updatePoint")
            return
        # compute normal
        self.dx = self.a[0] - self.b[0]
        self.dy = self.a[1] - self.b[1]
        self.normal = [-self.dy, self.dx]
        # compute magnitude of normal
        self.normMagnitude = np.sqrt(pow(self.normal[0], 2) + pow(self.normal[1], 2))
        # normalize normal
        self.normal[0] = self.normal[0] / self.normMagnitude
        self.normal[1] = self.normal[1] / self.normMagnitude
        # compute magnitude
        self.magnitude = np.sqrt(pow(self.dx, 2) + pow(self.dy, 2))
    
    def sharedPoint(self, lineB):
        if np.array_equal(self.a, lineB.b):
            return True
        elif np.array_equal(self.b, lineB.a):
            return True
        else:
            return False
    
    def perp(self, a) :
        b = np.empty_like(a)
        b[0] = -a[1]
        b[1] = a[0]
        return b
    
    # checks if two lines intersects or share a point
    # returns coordinates or None
    def intersect(self, lineB) :
        # if lines share a point
        if self.sharedPoint(lineB):
            return None
        # ensure points are arrays, not lists
        self.a = np.array(self.a)
        self.b = np.array(self.b)
        lineB.a = np.array(lineB.a)
        lineB.b = np.array(lineB.b)
        # calculate help variables
        da = self.b - self.a
        db = lineB.b - lineB.a
        dp = self.a - lineB.a
        dap = self.perp(da)
        denom = np.dot(dap, db)
        num = np.dot(dap, dp)
        # lines are not parallel
        if denom != 0:
            # calculate crossing point
            crossing = (num / denom.astype(float)) * db + lineB.a
            return crossing
        return None
    
    def contains(self, point):
        #find greatest and lowest X
        if self.a[0] < self.b[0]:
            lesserX = self.a[0]
            greaterX = self.b[0]
        else:
            lesserX = self.b[0]
            greaterX = self.a[0]
        #find greatest and lowest Y
        if self.a[1] < self.b[1]:
            lesserY = self.a[1]
            greaterY = self.b[1]
        else:
            lesserY = self.b[1]
            greaterY = self.a[1]
        #if point within ranges
        if (lesserX <= point[0] <= greaterX) and (lesserY <= point[1] <= greaterY):
            return True
        else:
            return False
    
    def moveSegment(self, moveDist):
        
        moveX = self.normal[0] * moveDist
        moveY = self.normal[1] * moveDist
        self.a = [self.a[0] + moveX, self.a[1] + moveY]
        self.b = [self.b[0] + moveX, self.b[1] + moveY]
        

        
