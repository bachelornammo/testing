'''
Created on 5. apr. 2017

@author: JonAnders
'''

import src.model2d.model.shapes as shapes
import src.model2d.controller.rotator as rotator

import moveLine

star = shapes.paraStar()
mover = moveLine.mover()
tank = shapes.tank()

def updateStar1(armLength, numArms, innerR, width, stepSize):
    global star, mover
    star = shapes.paraStar()
    mover = moveLine.mover()
    star.setLength(float(armLength), tank)
    star.setNumArms(float(numArms))
    star.setInnerRadius(float(innerR))
    star.setWidth(float(width))
    star.setPointDistance(float(stepSize))
    mover.setStepSize(float(stepSize))
    
def updateTank(tankwall):
    global tank
    tank = shapes.tank()
    tank.setOuterRadius(float(tankwall))

def expand(segments):
    mover.setSegments(segments)
    shape = mover.moveStep(star)
    return shape

def getTank(): return tank.getTankWall()
def rotate(shape): return rotator.rotationSeperateFigures(star.numArms, shape)
def getMover(): return mover
def getHalfArm(): return star.getStarShape()
def getIteration(): return mover.getIteration()
    
