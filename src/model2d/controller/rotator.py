'''
Created on 22. feb. 2017

@author: JonAnders
'''
import numpy as np
from testing.src.model2d.controller import nonClass


def rotationMatrix(n, shape):
    xs, ys = nonClass.getPoints(shape)
    length = xs.__len__()
    for x in range(0, length):
        xs.append(xs[length - 1 - x])
        ys.append(-ys[length - 1 - x])
    # prepare variables
    theta = np.pi / n
    matrix = [[np.cos(theta), -np.sin(theta)],
              [np.sin(theta), np.cos(theta)]]
    length = xs.__len__()
    prevRotXs = xs
    prevRotYs = ys
    # rotate full circle
    for arm in range(0, int(2 * n)):  # @UnusedVariable
        rotXs = []
        rotYs = []
        # mirror each point
        for x in range(0, length):
            point = [prevRotXs[x], prevRotYs[x]]
            point = np.dot(matrix, point)
            rotXs.append(point[0]); xs.append(point[0])
            rotYs.append(point[1]); ys.append(point[1])
        prevRotXs = rotXs
        prevRotYs = rotYs
    
    return nonClass.createLine(xs, ys)

def rotationSeperateFigures(n, shape):
    if shape.__len__() > 0:
        xs, ys = nonClass.getPoints(shape)
        length = xs.__len__()
        xss = []; yss = []
        xss.append(xs); yss.append(ys)
        prevRotX1 = xs; prevRotY1 = ys
        xs = []; ys = []
        for x in range(0, length):
            xs.append(prevRotX1[length-1-x])
            ys.append(-prevRotY1[length-1-x])
        xss.append(xs); yss.append(ys)
        prevRotX2 = xs; prevRotY2 = ys
        theta = np.pi / n
        matrix = [[np.cos(theta), -np.sin(theta)],
                  [np.sin(theta), np.cos(theta)]]
        for arm in range (0, int(2*n)): #@UnusedVariable
            rotX1 = []; rotX2 = []; xs = []; xs2 = []
            rotY1 = []; rotY2 = []; ys = []; ys2 = []
            for x in range(0, length):
                point1 = [prevRotX1[x], prevRotY1[x]]
                point2 = [prevRotX2[x], prevRotY2[x]]
                point1 = np.dot(matrix, point1)
                point2 = np.dot(matrix, point2)
                rotX1.append(point1[0]); xs.append(point1[0])
                rotY1.append(point1[1]); ys.append(point1[1])
                rotX2.append(point2[0]); xs2.append(point2[0])
                rotY2.append(point2[1]); ys2.append(point2[1])
            prevRotX1 = rotX1; prevRotX2 = rotX2
            prevRotY1 = rotY1; prevRotY2 = rotY2
            xss.append(xs); xss.append(xs2)
            yss.append(ys); yss.append(ys2)
        figures = []
        for x in range(0, xss.__len__()):
            figures.append(nonClass.createLine(xss[x], yss[x]))
        return figures