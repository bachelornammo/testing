'''
Created on 28. mars 2017

@author: Jon Anders
'''
from testing.src.model2d.model import lineSegment
import numpy as np
import matplotlib.pyplot as pp


def circle(x, r):
        return np.sqrt(pow(r, 2) - pow(x, 2))

# returns the angle between two lines
def checkAngle(lineA, lineB):
        A = lineA.magnitude
        B = lineB.magnitude
        c = lineSegment.lineSegment(lineA.a, lineB.b)
        C = c.magnitude
        cosOfC = (pow(A, 2) + pow(B, 2) - pow(C, 2)) / (2 * A * B)
        thetaC = np.arccos(cosOfC)
        return thetaC

# finds the index of the closest array element to value
def findNearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx

# returns the points in a array of lines
def getPoints(segments):
    xs = []
    ys = []
    xs.append(segments[0].a[0])
    ys.append(segments[0].a[1])
    for segment in segments:
        xs.append(segment.b[0])
        ys.append(segment.b[1])
    return [xs, ys]
    
def createLine(initXs, initYs):
    prevX = 0
    prevY = 0
    segments = []
    for i in range(1, initXs.__len__()):
        x = initXs[i]
        y = initYs[i]
        prevX = initXs[i-1]
        prevY = initYs[i-1]
        segment = lineSegment.lineSegment([prevX, prevY], [x, y])
        segments.append(segment)
    return segments
    
# checks if two floats are relatively close to each other
def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

# ensures all line segments are connected correctly
def segmentLengthControll(segments, maxdist):
    newSegments = []
    for index in range(1, segments.__len__()):
        #if current or last segment is too long
        if (segments[index].magnitude > maxdist or segments[index -1].magnitude > maxdist):
            #create new segment between existing ones
            if not segments[index -1].sharedPoint(segments[index]):
                #breaks the prew line in 2, to prevent always adding new lines
                #find middle of large line
                """
                splitX = segments[index -1].a[0] + (segments[index -1].dx / 2)
                splitY = segments[index -1].a[1] + (segments[index -1].dy / 2)
                splitPoint = [splitX, splitY]
                segments[index -1].updatePoint('b', splitPoint)
                line = lineSegment.line(segments[index -1].b, segments[index].a)
                newSegments.append([line, index])
               """ 
                line = lineSegment.lineSegment(segments[index -1].b, segments[index].a)
                newSegments.append([line, index])
               
            
        else:   
            crossing = segments[index -1].intersect(segments[index])
            if (crossing is not None):
                segments[index].updatePoint('a', crossing)
                segments[index -1].updatePoint('b', crossing)
    for item in reversed(newSegments):
        line = item[0]
        index = item[1]
        segments = np.insert(segments, index, line)
    return segments

    # checks if angel is too sharp and creates an tie shape
def segmentOverlapControll(segments):
    #for each segment
    droppedSegments = []
    segments = np.array(segments)
    for i in range(0, segments.__len__()):
        #check against all other segments
        for j in range(0, segments.__len__()):
            #not rechecking segments
            if segments[i] < segments[j]:
                intersection = segments[i].intersect(segments[j])
                if intersection is not None:
                    #check if crossing is along both lines
                    if segments[i].contains(intersection) and segments[j].contains(intersection):
                        segments[i].updatePoint('b', intersection)
                        segments[j].updatePoint('a', intersection)
                        #save segments between i and j
                        droppedSegments.extend(range(i+1, j))

    for x in reversed(droppedSegments):
        segments = np.delete(segments, x)
    return segments
    
def symmetryLineControll(segments, theta, outerR):
    droppedSegments = None
    for i in range (0, segments.__len__()):
        ax = segments[i].a[0]
        ay = -segments[i].a[1]
        thetaA = np.arctan2(ay, ax)
        bx = segments[i].b[0]
        by = -segments[i].b[1]
        thetaB = np.arctan2(by, bx)
        if thetaA > theta and thetaB > theta:
            if droppedSegments is None or i > droppedSegments:
                droppedSegments = i
        elif thetaA > theta:
            symLine = lineSegment.lineSegment([0,0], [outerR * np.cos(-theta), outerR * np.sin(-theta)])
            crossing = segments[i].intersect(symLine)
            segments[i].updatePoint('a', crossing)
            if droppedSegments is not None or i > droppedSegments:
                droppedSegments = i
        elif thetaB > theta:
            symLine = lineSegment.lineSegment([0,0], [outerR * np.cos(-theta), outerR * np.sin(-theta)])
            crossing = segments[i].intersect(symLine)
            segments[i].updatePoint('b', crossing)
            if droppedSegments is not None or i > droppedSegments:
                droppedSegments = i
    if droppedSegments is not None:
        for x in range(droppedSegments-1, -1, -1): #from from ds-1 to 0, stepsize -1
            segments = np.delete(segments, x)
    return segments

def tankWallControll(segments, outerR):
    droppedSegments = None
    for i in range(0, segments.__len__()):
        a = segments[i].a
        b = segments[i].b
        #length from origin to each point
        ar = np.sqrt(pow(a[0], 2) + pow(a[1], 2))
        br = np.sqrt(pow(b[0], 2) + pow(b[1], 2))
        #drop segment if both points are outside the tank wall
        if ar > outerR and br > outerR:
            if droppedSegments is None or i < droppedSegments:
                droppedSegments = i
        #if one point is outside the tank wall, move it to the crossing
        elif ar > outerR or br > outerR:
            x = circleCrossing(segments[i], outerR)
            crossing = [x*a[0] + (1-x)*b[0],
                        x*a[1] + (1-x)*b[1]]
            if ar > br:
                segments[i].updatePoint('a', crossing)
            else:
                segments[i].updatePoint('b', crossing)
    if droppedSegments is not None:
        for x in reversed(range(droppedSegments, segments.__len__())):
            segments = np.delete(segments, x)
    return segments

def circleCrossing(line, r):
    p1 = line.a
    p2 = line.b
    #x^2*|p1|^2 + (x^2-2x+1)*|p2|^2 + 2x(1-x)*p1*p2 = R^2
    # = x^2*|p1|^2 + x^2*|p2|^2 - 2x^2*p1*p2 -2x*|p2|^2 + 2x*p1*p2 + |p2|^2 - R^2
    
    dotP1 = abs(p1[0]*p1[0] + p1[1]*p1[1])
    dotP2 = abs(p2[0]*p2[0] + p2[1]*p2[1])
    dotP1P2 = p1[0]*p2[0] + p1[1]*p2[1]
    
    #x^2*a + x*b + c - r = 0
    a = dotP1 + dotP2 - 2*dotP1P2
    b = -2*dotP2 + 2*dotP1P2
    c = dotP2 - pow(r,2)
    
    #x = (-b+-sqrt(b^2-4ac) / 2a
    alpha1 = (-b + np.sqrt(pow(b,2)-4*a*c)) / (2*a)
    alpha2 = (-b - np.sqrt(pow(b,2)-4*a*c)) / (2*a)
    if -1 <= alpha1 <= 1:
        return alpha1
    else:
        return alpha2

def isDone(shape, outerR):
    for segment in shape:
        ra = np.sqrt(pow(segment.a[0], 2) * pow(segment.a[1], 2))
        rb = np.sqrt(pow(segment.b[0], 2) * pow(segment.b[1], 2))
        if ra != outerR and rb != outerR:
            return False
    return True

def calculatePerimeter(segments, outerR):
    segmentsToDelete = []
    perimeter = []
    for index in range(0, segments.__len__()):
        ax = segments[index].a[0]
        ay = segments[index].a[1]
        bx = segments[index].b[0]
        by = segments[index].b[1]
        if ( np.sqrt(pow(ax, 2) + pow(ay, 2)) == outerR and np.sqrt(pow(bx, 2) + pow(by, 2)) == outerR): 
            segmentsToDelete.append(segments[index])
        else:
            perimeter += segments[index].magnitude()             
    
    for index in reversed(segmentsToDelete):
        np.delete(segments, index)
    

    return segments

def getLength(segments):
    totLen = 0
    for seg in segments:
        totLen += seg.magnitude
    return totLen

def getPortArea(segments, outerR, innerR, numArms):
    #prep segment list by including the 0,0 point
    lastPoint = segments[-1].b
    firstPoint = segments[0].a
    #check if line crosses entire sector
    theta = np.arctan2(lastPoint[1], lastPoint[0])
    if theta != 0:
        x1 = lastPoint[0]
        x2 = outerR
        ys = []; xs = []
        #from x1 to x2
        for x in np.arange(x1, x2, segments[-1].magnitude):
            y = -circle(x, innerR)
            ys.append(y)
            xs.append(x)
        for i in range(0, xs.__len__()):
            if i == 0:
                None
            else:
                segments = np.append(segments, lineSegment.lineSegment([xs[i-1],ys[i-1]], [xs[i], ys[i]]))
    lastPoint = segments[-1].b
    segments = np.append(segments, lineSegment.lineSegment(lastPoint, [0,0]))
    segments = np.append(segments, lineSegment.lineSegment([0,0], firstPoint))
    #prepare calculation of area
    x1 = 0; prevX = 0
    y1 = 0; prevY = 0
    pos = 0
    neg = 0
    for seg in segments:
        if x1 == 0 and y1 == 0:
            x1 = seg.a[0]; prevX = x1
            y1 = seg.a[1]; prevY = y1
        pos += (prevX*seg.b[1])
        neg += (prevY*seg.b[0])
        prevX = seg.b[0]
        prevY = seg.b[1]
    pos += (prevX*y1)
    neg += (prevY*x1)
    #formula for area given all corners of the figure
    #A = 0.5*abs((x1*y2 + x2*y2 .. xn*y1) - (x2*y1 + x3*y2 .. x1*yn))
    for segment in segments:
        pp.plot(segment.getX(), segment.getY())
    x_window = -10 , 10
    y_window = -10, 10
    pp.xlim(*x_window)
    pp.ylim(*y_window)
    pp.show()
    return 0.5* abs(pos-neg) * numArms * 2

def getWebArea(portArea, outerR):
    outerA = np.pi * pow(outerR, 2)
    return outerA - portArea

"""
#TEST OF PORT AREA
A = [0,0]
B = [0,3]
C = [2,2]
D = [2,0]
seg1 = lineSegment.lineSegment(A, B)
seg2 = lineSegment.lineSegment(B, C)
seg3 = lineSegment.lineSegment(C, D)
seg4 = lineSegment.lineSegment(D, A)
segs = [seg1, seg2, seg3, seg4]

print getPortArea(segs)
"""