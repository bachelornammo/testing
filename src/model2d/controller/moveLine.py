'''
Created on 27. feb. 2017

@author: JonAnders
'''


from src.model2d.model import lineSegment
from src.model2d.model import shapes # @UnusedImport (passed as argument to drawStep)
from src.model2d.controller import nonClass
       
class mover:
    def __init__(self):
        self.segments = []
        self.stepSize = 0
        self.iteration = 0

    
    def setSegments(self, segments):
        self.segments = segments
    #takes a list of points and creates line segments for each pair
    
    def createLine(self, initXs, initYs):
        prevX = 0
        prevY = 0
        for i in range(1, initXs.__len__()):
            x = initXs[i]
            y = initYs[i]
            prevX = initXs[i - 1]
            prevY = initYs[i - 1]
            segment = lineSegment.lineSegment([prevX, prevY], [x, y])
            self.segments.append(segment)

    
    def moveStep(self, shape):
        self.iteration += 1
        newSegments = []
        # move all segments
        for x in range(0, self.segments.__len__()):
            segment = self.segments[x]
            # move linesegment mveDist length along normal
            segment.moveSegment(self.stepSize)
            newSegments.append(segment)
        
        line = lineSegment.lineSegment(segment.b, [shape.length + self.stepSize*self.iteration, 0])
        newSegments.append(line)
        # check that all segments are connected and not ovelapping
        self.segments = nonClass.segmentOverlapControll(newSegments)
        self.segments = nonClass.segmentLengthControll(self.segments, shape.pointDistance * 2)
        self.segments = nonClass.symmetryLineControll(self.segments, shape.theta/2, shape.outerR) #Check symmetri line
        self.segments = nonClass.tankWallControll(self.segments, shape.outerR) #Check R vs outerR
        return self.segments
            
    def setStepSize(self, size):
        self.stepSize = size
    def setIteration(self, num):
        self.iteration = num
    def getIteration(self):
        return self.iteration
        


