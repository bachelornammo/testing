import matplotlib
import model2d.controller.nonClass as nonClass
matplotlib.use('TkAgg')
from matplotlib.figure import Figure
import src.model2d.controller.flowControll as fc
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
import Tkinter as Tk
import xlsxwriter
import numpy as np

root = Tk.Tk()
root.wm_title("NAMMO - Solidfuel Simulator")

f = Figure(figsize=(5, 4), dpi=100)
a = f.add_subplot(111)

# a tk.DrawingArea
canvas = FigureCanvasTkAgg(f, master=root)
canvas.get_tk_widget().pack(side=Tk.RIGHT, fill=Tk.BOTH, expand=1)

#toolbar = NavigationToolbar2TkAgg(canvas, root)
#toolbar.update()
#canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)


# Change number of arms on star
label = Tk.Label(root, text="Change number of arms").pack()  
numArms = Tk.Entry(root)
numArms.insert(0, "4")
numArms.pack()

# Change inner diameter
label = Tk.Label(root, text="Change inner diameter").pack()  
innerR = Tk.Entry(root)
innerR.insert(0, "2")
innerR.pack()

# Change outer diameter
label = Tk.Label(root, text="Change outer diameter").pack()  
outerR = Tk.Entry(root)
outerR.insert(0, "5")
outerR.pack()   

# Change arm width
label = Tk.Label(root, text="Change arm width").pack()  
armWidth = Tk.Entry(root)
armWidth.insert(0, "2")
armWidth.pack()   

# Change arm length
label = Tk.Label(root, text="Change arm length").pack()  
armLength = Tk.Entry(root)
armLength.insert(0, "4")
armLength.pack()   

#change step size
label = Tk.Label(root, text="Change step size").pack()  
stepSize = Tk.Entry(root)
stepSize.insert(0, "0.5")
stepSize.pack() 
 
def ChangeVarSize():
    fc.updateTank(outerR.get())
    fc.updateStar1(armLength.get(), numArms.get(), innerR.get(),
                   armWidth.get(), stepSize.get())
    
def clearArea():
    #clear drawing area and plot new figures
    a.clear()
    
def drawShape(shape):
    for segment in shape:
        a.plot(segment.getX(), segment.getY(), 'g')

def annote(shape):
    i = 0
    xs = []
    ys = []
    for segment in shape:
        xs.append(segment.getX())
        ys.append(segment.getY())
    for xy in zip(xs,ys):
        a.annotate('(%s)' % i, xy=xy, textcoords='data')
        i += 1

def update(): 
    clearArea()
    ChangeVarSize()
    drawShape(fc.getTank())
    #drawShape(fc.rotate(fc.getHalfArm()))
    rotateDraw(fc.getHalfArm())
    expand()
    canvas.show()
    
def expand():
    stats = []
    done = False
    shape = fc.getHalfArm()
    outerRadius = float(outerR.get())
    innerRadius = float(innerR.get())
    totalArea = np.pi * pow(outerRadius, 2)
    numOfArms = float(numArms.get())
    portArea = nonClass.getPortArea(shape, outerRadius, innerRadius, numOfArms)
    stats.append([fc.getIteration(), nonClass.getLength(shape), portArea, nonClass.getWebArea(portArea, outerRadius), totalArea])
    while(not done):
        shape = fc.expand(shape)
        if shape.__len__() > 0:
            rotateDraw(shape)
            portArea = nonClass.getPortArea(shape, outerRadius, innerRadius, numOfArms)
            stats.append([fc.getIteration(), nonClass.getLength(shape), portArea, nonClass.getWebArea(portArea, outerRadius), totalArea])
        else:
            done = True
        #annote(shape)
    printExcel(stats)

def printExcel(stats):
    workbook = xlsxwriter.Workbook('test1.xlsx', options={'nan_inf_to_errors': True})
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({'bold': True})
    row = 1
    col = 0
    worksheet.write('A1', 'Web', bold)
    worksheet.write('B1', 'Perimeter', bold)
    worksheet.write('C1', 'PortArea', bold)
    worksheet.write('D1', 'WebArea', bold)
    worksheet.write('E1', 'TotalArea', bold)
    for stat in stats:
        worksheet.write(row, col, stat[0])
        worksheet.write(row, col+1, stat[1])
        worksheet.write(row, col+2, stat[2])
        worksheet.write(row, col+3, stat[3])
        worksheet.write(row, col+4, stat[4])
        row+=1
    workbook.close()

def rotateDraw(shape):
    figures = fc.rotate(shape)
    for f in figures:
        drawShape(f)
    
button1 = Tk.Button(master=root, text="submit", command=update)
button1.pack()


def _quit():
    root.quit()     # stops mainloop
    root.destroy()  # this is necessary on Windows to prevent
                    # Fatal Python Error: PyEval_RestoreThread: NULL tstate

button = Tk.Button(master=root, text='Quit', command=_quit)
button.pack(side=Tk.BOTTOM)
update()
Tk.mainloop()
# If you put root.destroy() here, it will cause an error if
# the window is closed *with the window manager.